package com.first.appaccount.presentation.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "movie_table")
data class MovieInternet(

    @PrimaryKey(autoGenerate = true)
    @SerializedName("id")
    val id: Int? = null,

    @ColumnInfo
    @SerializedName ("overview")
    val overview: String? = null,

    @ColumnInfo
    @SerializedName ("poster_path")
    val poster_path: String? = null,

    @ColumnInfo
    @SerializedName ("release_date")
    val release_date: String? = null,

    @ColumnInfo
    @SerializedName("title")
    val title: String? = null,

    @ColumnInfo
    @SerializedName("budget")
    val budget: Int? = null,

    @ColumnInfo
    @SerializedName("popularity")
    val popularity: Double? = null,



//    val production_countries: List<ProductionCountry>,
//    val vote_average: Double? = null,
//    val adult: Boolean,
//    val backdrop_path: String,
//    val belongs_to_collection: Any,
//    val genres: List<Genre>?=null,
//    val homepage: String,
//    val imdb_id: String,
//    val original_language: String,
//    val original_title: String,
//    val production_companies: List<ProductionCompany>,
//    val revenue: Int,
//    val runtime: Int,
//    val spoken_languages: List<SpokenLanguage>,
//    val status: String,
//    val tagline: String,
//    val video: Boolean,
//    val vote_count: Int
)



