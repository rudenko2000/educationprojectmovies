package com.first.appaccount.presentation.models

import com.google.gson.annotations.SerializedName

data class Images(
    @SerializedName("secure_base_url")
    val secure_base_url: String? = null
)
