package com.first.appaccount.presentation.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.annotations.SerializedName

data class MoviesList(

    @SerializedName("results")
    val movieList: List<MovieInternet>
)
