package com.first.appaccount.presentation.models


import com.google.gson.annotations.SerializedName

data class ObjectImage(
    @SerializedName("images")
    val images: Images,
)
