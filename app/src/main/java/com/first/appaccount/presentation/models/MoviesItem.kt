package com.first.appaccount.presentation.models


data class MoviesItem(
    val id: Int?,
    val titleText: String="",
    val image_id: String?,
    val description: String?,
    val releaseDate: String?,
    val budget: Int?,
    val popularity: Double?,
)
