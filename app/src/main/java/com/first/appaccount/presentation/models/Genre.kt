package com.first.appaccount.presentation.models

data class Genre(
    val id: Int,
    val name: String
)