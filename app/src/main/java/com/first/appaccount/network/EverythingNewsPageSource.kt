//package com.first.appaccount.network
//
//import androidx.paging.PagingSource
//import androidx.paging.PagingState
//import com.first.appaccount.presentation.models.MovieInternet
//import com.first.appaccount.retrofit.QuestApi
//import retrofit2.HttpException
//
//class EverythingNewsPageSource(
//    private val newService: QuestApi,
//    private val query: String,
//) : PagingSource<Int, MovieInternet>() {
//    override fun getRefreshKey(state: PagingState<Int, MovieInternet>): Int? {
//        TODO("Not yet implemented")
//    }
//
//    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MovieInternet> {
//        if(query.isEmpty()) {
//            return LoadResult.Page(emptyList(), prevKey = null, nextKey = null)
//        }
//        val page: Int = params.key ?: 1
//            val pageSize: Int = params.loadSize
//
//        val response = newService.getMovies(query, page, pageSize)
//        if (response.isSuccessful) {
//
//        }
//    }
//}