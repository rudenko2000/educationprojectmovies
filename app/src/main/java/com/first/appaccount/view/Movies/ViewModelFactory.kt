package com.first.appaccount.view.Movies

import android.app.Application
import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.first.appaccount.db.MovieDataBase
import com.first.appaccount.repository.MySharedPreferences
import com.first.appaccount.repository.MySharedPreferences.Companion.PREFERENCE_KEY
import com.first.appaccount.repository.RepositoryInternetImpl
import com.first.appaccount.retrofit.QuestApi
import com.first.appaccount.view.DescriptionMovies.DescriptionViewModel

class ViewModelFactory(
    private val app: Application,
) : ViewModelProvider.Factory {


    private val daoMovie = MovieDataBase.getInstance(app)
    private val sharedPref = app.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE)
    private val storage = MySharedPreferences(sharedPref)

    private var repository = RepositoryInternetImpl(QuestApi.create(), daoMovie, storage)
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when (true) {

            modelClass.isAssignableFrom(MoviesViewModel::class.java) ->
                MoviesViewModel(this.repository) as T

            modelClass.isAssignableFrom(DescriptionViewModel::class.java) ->
                DescriptionViewModel(this.repository) as T

            else -> throw IllegalArgumentException("ViewModel Not Found")

        }
    }
}
