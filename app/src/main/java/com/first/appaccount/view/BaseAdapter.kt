package com.first.appaccount.view

import android.view.View
import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<V> : RecyclerView.Adapter<BaseAdapter.BaseViewHolder<V>>() {

    private val listItem: MutableList<V> = mutableListOf()

    override fun onBindViewHolder(holder: BaseViewHolder<V>, position: Int) {
        holder.bind(listItem[position])
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    fun replaceData(data: List<V>) {
        listItem.clear()
        listItem.addAll(data)
        notifyDataSetChanged()
    }

    abstract class BaseViewHolder<V>(view: View) : RecyclerView.ViewHolder(view) {

        var item: V? = null
            private set

        @CallSuper
        open fun bind(listItem: V) {
            this.item = listItem
        }
    }
}