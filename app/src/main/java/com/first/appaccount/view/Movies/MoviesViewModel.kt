package com.first.appaccount.view.Movies

import android.app.Application
import androidx.lifecycle.*
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MoviesViewModel(
    private val repository: Repository,
) : ViewModel() {

    private var moviesList: List<MovieInternet>? = null

    private val _baseImageUrl: MutableLiveData<String> = MutableLiveData()
    val baseImageUrl: LiveData<String>
        get() = _baseImageUrl

    fun getAllMoviesRoom(): LiveData<List<MovieInternet>> {
        return repository.allMoviesRoom()
    }

    private fun saveDataToRoom(list: List<MovieInternet>?) {
        viewModelScope.launch(Dispatchers.IO) {
            if (list != null) {
                repository.insertMovieRoom(list)
            }
        }
    }

    fun getBaseUrl() {

        viewModelScope.launch(Dispatchers.IO) {
            val url = repository.getImageBaseUrl()
            withContext(Dispatchers.Main) {
                _baseImageUrl.value = url
            }
        }
    }

    fun setImageBaseUrl() {

        viewModelScope.launch(Dispatchers.IO) {
            try {
                repository.setImageBaseUrl()
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun getMoviesRetrofit() {

        viewModelScope.launch(Dispatchers.IO) {
            try {
                moviesList = repository.getMoviesRetrofit()
                withContext(Dispatchers.Main) {
                    saveDataToRoom(moviesList)
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    e.printStackTrace()
                }
            }
        }
    }
}

