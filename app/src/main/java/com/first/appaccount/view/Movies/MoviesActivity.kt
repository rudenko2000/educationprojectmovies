package com.first.appaccount.view.Movies

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.view.DescriptionMovies.DescriptionActivity


class MoviesActivity : AppCompatActivity() {

    lateinit var recyclerView: RecyclerView
    private val adapter = MoviesAdapter {
        onMoviesClicked(it)
    }
    lateinit var viewModel: MoviesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.first.appaccount.R.layout.activity_movies)

        viewModel = ViewModelProvider(
            this,
            ViewModelFactory(application)
        )[MoviesViewModel::class.java]
        recyclerView = findViewById(com.first.appaccount.R.id.statisticRv)
        recyclerView.adapter = adapter

        viewModel.baseImageUrl.observe(this) { url ->
            adapter.replaceBaseUrl(url)
        }

        viewModel.getAllMoviesRoom().observe(this) { list ->
            list.asReversed()
            adapter.replaceData(list)
        }

        viewModel.getMoviesRetrofit()
        viewModel.getBaseUrl()
        viewModel.getAllMoviesRoom()
        viewModel.setImageBaseUrl()
    }

    private fun onMoviesClicked(moviesItem: MovieInternet) {
        val intent = DescriptionActivity.getLauncherIntent(this, moviesItem.id)
        startActivity(intent)
    }
}