package com.first.appaccount.view.DescriptionMovies

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.first.appaccount.R
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.view.Movies.ViewModelFactory


class DescriptionActivity : AppCompatActivity() {

    private lateinit var tvTitle: TextView
    private lateinit var tvContent: TextView
    private lateinit var im: ImageView
    private lateinit var tvPopularity: TextView
    private lateinit var tvBudget: TextView
    private lateinit var tvDateRelease: TextView
    private lateinit var baseUrl: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_description_movies)
        val id = intent.getIntExtra("id", -1)
        val viewModel = ViewModelProvider(
            this,
            ViewModelFactory(application)
        )[DescriptionViewModel::class.java]

        viewModel.getBaseUrl()
        viewModel.baseImageUrl.observe(this) { url ->
            baseUrl = url
        }

        viewModel.getMoviesDetails(id)
        viewModel.myMovie.observe(this) { item ->

            tvTitle = findViewById(R.id.tvTitle)
            tvContent = findViewById(R.id.tvContent1)
            im = findViewById(R.id.im)
            tvPopularity = findViewById(R.id.tvPopularity1)
            tvBudget = findViewById(R.id.tvBudget1)
            tvDateRelease = findViewById(R.id.tvReleaseDate1)
            bind(item)
        }
    }

    private fun bind(movie: MovieInternet) {
        tvTitle.text = movie.title
        tvContent.text = movie.overview
        tvPopularity.text = movie.popularity.toString()
        tvBudget.text = movie.budget.toString()
        tvDateRelease.text = movie.release_date.toString()

        val imageUrl = baseUrl + "original" + movie.poster_path
        Glide.with(this)
            .load(imageUrl)
            .into(im)
    }

    companion object {
        fun getLauncherIntent(context: Context, id: Int?): Intent {
            val intent = Intent(context, DescriptionActivity::class.java)
            intent.putExtra("id", id)
            return intent
        }
    }
}