package com.first.appaccount.view.DescriptionMovies

import android.app.Application
import androidx.lifecycle.*
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.repository.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DescriptionViewModel(
    private val repository: Repository,
) : ViewModel() {

    val myMovie: MutableLiveData<MovieInternet> = MutableLiveData()

    private val _baseImageUrl: MutableLiveData<String> = MutableLiveData()
    val baseImageUrl: LiveData<String>
        get() = _baseImageUrl

    fun getMoviesDetails(id: Int) {

        viewModelScope.launch(Dispatchers.IO) {

            try {
                val moviesById = repository.getMoviesDetails(id)
                withContext(Dispatchers.Main) {
                    myMovie.value = moviesById
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    e.printStackTrace()
                }
            }
        }
    }

    fun getBaseUrl() {

        viewModelScope.launch(Dispatchers.IO) {
            try {
                val imageUrl = repository.getImageBaseUrl()
                withContext(Dispatchers.Main) {
                    _baseImageUrl.value = imageUrl
                }
            } catch (e: Exception) {
                withContext(Dispatchers.Main) {
                    e.printStackTrace()
                }
            }
        }
    }
}



