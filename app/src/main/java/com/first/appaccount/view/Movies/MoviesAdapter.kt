package com.first.appaccount.view.Movies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.first.appaccount.R
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.view.BaseAdapter

class MoviesAdapter(
    private val onMoviesClick: ((MovieInternet) -> Unit),
) : BaseAdapter<MovieInternet>() {

    private var baseUrlImage: String? = null

    fun replaceBaseUrl(url: String) {
        baseUrlImage = url
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<MovieInternet> {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_film, parent, false)
        return MoviesViewHolder(itemView, onMoviesClick)
    }

    inner class MoviesViewHolder(
        view: View,
        onMoviesClick: ((MovieInternet) -> Unit),
    ) : BaseViewHolder<MovieInternet>(view) {

        private val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
        private val tvContent = view.findViewById<TextView>(R.id.tvContent)
        private val releaseDateTv = view.findViewById<TextView>(R.id.release_date)
        private val popularityTv = view.findViewById<TextView>(R.id.popularity)

        init {
            itemView.setOnClickListener { item?.let { onMoviesClick(it) } }
        }

        override fun bind(listItem: MovieInternet) {
            super.bind(listItem)

            val imageUrl = baseUrlImage + "original" + listItem.poster_path

            Glide.with(itemView.context)
                .load(imageUrl)
                .into(itemView.findViewById(R.id.imMain))

            tvTitle.text = listItem.title
            val textShort = listItem.overview
            tvContent.text = textShort
            releaseDateTv.text = listItem.release_date
            popularityTv.text = listItem.popularity.toString()

        }
    }
}