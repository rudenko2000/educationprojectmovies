package com.first.appaccount.repository


import androidx.lifecycle.LiveData
import com.first.appaccount.db.MovieDataBase
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.retrofit.QuestApi

class RepositoryInternetImpl(
    private var questApi: QuestApi,
    val dataBase: MovieDataBase,
    private val storage: MySharedPreferences,
) : Repository {


    override suspend fun getMoviesRetrofit(): List<MovieInternet> {
        val quest1 = questApi.getMovies("ru-Ru", 11, "us")
        return quest1.movieList
    }

    override suspend fun getMoviesDetails(id: Int): MovieInternet {
        //return questApi.getMoviesDetails(id, "ru-Ru")
        return dataBase.getMovieDao().getMoviesById(id)
    }

    override suspend fun getImageBaseUrl(): String {
        return storage.getBaseImageUrl()
    }

    override suspend fun setImageBaseUrl() {
        val baseUrl = questApi.getBaseUrl()
        storage.setBaseImageUrl(baseUrl.images.secure_base_url)
    }

    override fun allMoviesRoom(): LiveData<List<MovieInternet>> {
        return dataBase.getMovieDao().getAllMovies()
    }

    override suspend fun insertMovieRoom(movieInternet: List<MovieInternet>) {
        dataBase.getMovieDao().insert(movieInternet)
    }

    override suspend fun deleteMovieRoom(movieInternet: MovieInternet) {
        dataBase.getMovieDao().delete(movieInternet)
    }
}

