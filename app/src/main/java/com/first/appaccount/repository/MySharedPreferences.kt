package com.first.appaccount.repository


import android.content.SharedPreferences

class MySharedPreferences(
    private val storage: SharedPreferences,
) {

    private val editor: SharedPreferences.Editor = storage.edit()

    fun getBaseImageUrl(): String {
        return storage.getString(BASE_API_KEY, "").toString()
    }

    fun setBaseImageUrl(baseUrl: String?) {
        if(baseUrl !== null) {
            editor.putString(BASE_API_KEY, baseUrl)
            editor.commit()
        }
    }

    companion object {
        const val BASE_API_KEY = "base_url"
        const val PREFERENCE_KEY = "preference_key"
    }
}