package com.first.appaccount.repository

import androidx.lifecycle.LiveData
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.presentation.models.ObjectImage

interface Repository {

    suspend fun getMoviesRetrofit(): List<MovieInternet>
    suspend fun getMoviesDetails(id: Int): MovieInternet
    suspend fun getImageBaseUrl(): String
    suspend fun setImageBaseUrl()
    fun allMoviesRoom(): LiveData<List<MovieInternet>>
    suspend fun insertMovieRoom(movieInternet: List<MovieInternet>)
    suspend fun deleteMovieRoom(movieInternet: MovieInternet)

}