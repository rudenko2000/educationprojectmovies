package com.first.appaccount.retrofit

import androidx.annotation.IntRange
import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.presentation.models.MoviesList
import com.first.appaccount.presentation.models.ObjectImage
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface QuestApi {


    @GET("movie/{id}")
    suspend fun getMoviesDetails(
        @Path("id") id: Int,
        @Query("language") language: String
    ): MovieInternet


    @GET("movie/popular")
    suspend fun getMovies(
        @Query("language") language: String,
        @Query("page") page: Int,
       // @Query("pageSize")  pageSize: Int,
        @Query("region") region: String
    ): MoviesList

    @GET("configuration")
    suspend fun getBaseUrl(): ObjectImage

    companion object {
        private const val baseUrl = "https://api.themoviedb.org/3/"
        private const val myApiKey = "f19d673a29c10ff8a3d52cc1c4f41309"
        private const val apiKey = "api_key"
        fun create(): QuestApi {
            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient = OkHttpClient.Builder()
                .addInterceptor { chain ->
                    val original = chain.request()
                    val originalHttpUrl = original.url
                    val url = originalHttpUrl.newBuilder()
                        .addQueryParameter(apiKey, myApiKey)
                        .build()
                    val requestBuilder: Request.Builder = original.newBuilder()
                        .url(url)

                    val request = requestBuilder.build()
                    chain.proceed(request)
                }.addInterceptor(httpLoggingInterceptor)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

            var questApi: QuestApi = retrofit.create(QuestApi::class.java)
            return questApi
        }
    }
}