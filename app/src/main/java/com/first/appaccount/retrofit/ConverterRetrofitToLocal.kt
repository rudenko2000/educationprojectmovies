package com.first.appaccount.retrofit

import com.first.appaccount.presentation.models.MovieInternet
import com.first.appaccount.presentation.models.MoviesItem

class ConverterRetrofitToLocal {

    companion object {
        fun converter(movie: MovieInternet): MoviesItem {

            return MoviesItem(
                id = movie.id,
                titleText = movie.title?:"",
                image_id = movie.poster_path,
                description = movie.overview,
                releaseDate = movie.release_date,
                budget = movie.budget,
                popularity = movie.popularity,
            )
        }
    }
}