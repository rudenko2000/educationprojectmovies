package com.first.appaccount.db

import android.content.Context
import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.first.appaccount.presentation.models.MovieInternet


@Database(
    entities = [MovieInternet::class],
    version = 5,
    exportSchema = true,
    autoMigrations = [AutoMigration(from = 4, to = 5)]
)
abstract class MovieDataBase : RoomDatabase() {

    abstract fun getMovieDao(): MovieDao

    companion object {
        private var database: MovieDataBase? = null

        @Synchronized
        fun getInstance(context: Context): MovieDataBase {
            return if (database == null) {
                database = Room.databaseBuilder(context, MovieDataBase::class.java, "db").build()
                database as MovieDataBase
            } else database as MovieDataBase
        }
    }
}