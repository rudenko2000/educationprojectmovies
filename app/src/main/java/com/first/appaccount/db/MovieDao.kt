package com.first.appaccount.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.first.appaccount.presentation.models.MovieInternet

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(movieInternet: List<MovieInternet>)

    @Delete
    suspend fun delete(movieInternet: MovieInternet)


    @Query("SELECT * from movie_table")
    fun getAllMovies(): LiveData<List<MovieInternet>>


    @Query("SELECT * FROM movie_table WHERE id = :employeeId")
    fun getMoviesById(employeeId: Int): MovieInternet
}